#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE 
#define _CRT_NONSTDC_NO_DEPRECATE

#include "OutStream.h"
#include <stdio.h>

//Initialazation function for out stream.
OutStream::OutStream()
{
	this->file = stdout;
}

//Delete function for out stream.
OutStream::~OutStream()
{
}

//Operator << for string (stream out).
OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->file, "%s", str);
	return *this;
}

//Operator << for number (stream out).
OutStream& OutStream::operator<<(int num)
{
	fprintf(this->file, "%d", num);
	return *this;
}
//Operator << for file (stream out).
OutStream& OutStream::operator<<(void(*pf)(FILE *))
{
	pf(this->file);
	return *this;
}

//End line function.
void endline(FILE * file)
{
	fprintf(file, "\n");
}
