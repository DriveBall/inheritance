#include "Logger.h"

//Initialazation function for logger.
Logger::Logger()
{
	this->os = OutStream();
	this->_startLine = true;
}

//Deletefunction for logger.
Logger::~Logger()
{
}

//Operator << for string (logger).
Logger & operator<<(Logger & l, const char * msg)
{
	l.setStartLine();
	l.os << msg;
	return l;
}

//Operator << for number (logger).
Logger & operator<<(Logger & l, int num)
{
	l.setStartLine();
	l.os << num;
	return l;
}

//Operator << for file (logger).
Logger & operator<<(Logger & l, void(*pf)(FILE *))
{
	l.setStartLine();
	l._startLine = true;
	l.os<< pf;
	return l;
}

void Logger::setStartLine()
{
	if (this->_startLine)
	{
		printf("Log %d: ", logCount);
		this->_startLine = false;
		logCount++;
	}
}