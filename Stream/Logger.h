#pragma once

#include "OutStream.h"
#include <stdio.h>

class Logger
{
	OutStream os;
	bool _startLine;
	void setStartLine();
public:
	Logger();													//Initialazation function for logger.
	~Logger();													//Delete function for logger.

	friend Logger& operator<<(Logger& l, const char *msg);		//Operator << for string (logger).	
	friend Logger& operator<<(Logger& l, int num);				//Operator << for number (logger).	
	friend Logger & operator<<(Logger & l, void(*pf)(FILE *));	//Operator << for file (logger).	
};

static int logCount = 1;
