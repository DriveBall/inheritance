#pragma once
#include "OutStream.h"

class FileStream : public OutStream
{

public:
	FileStream(char* str);	//Initialazation function for file stream.
	~FileStream();			//Delete function for file stream.

};
