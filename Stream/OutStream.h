#pragma once
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

class OutStream
{
protected:
	FILE * file;

public:
	OutStream();								//Initialazation function for out stream.
	~OutStream();								//Delete function for out stream.

	OutStream& operator<<(const char *str);		//Operator << for string (stream out).		
	OutStream& operator<<(int num);				//Operator << for number (stream out).
	OutStream& operator<<(void(*pf)(FILE *));	//Operator << for file (stream out).
};

void endline(FILE * file);
