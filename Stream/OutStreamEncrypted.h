#pragma once
#include "OutStream.h"

class OutStreamEncrypted : public OutStream
{
	int shift;
	char* encrypt(const char cipher[], int shift);
public:
	OutStreamEncrypted(int shift);						//Initilazation function for encrpyted stream out.
	~OutStreamEncrypted();								//Delete function for encrpyted stream out.

	OutStreamEncrypted& operator<<(const char *str);	//Operator << for string (encrpyted stream out).
	OutStreamEncrypted& operator<<(int num);			//Operator << for number (encrpyted stream out).
	OutStreamEncrypted& operator<<(void(*pf)(FILE *));	//Operator << for file   (encrpyted stream out).
};
