#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE 
#define _CRT_NONSTDC_NO_DEPRECATE
#include "OutStreamEncrypted.h"

//Initilazation function for encrpyted stream out.
OutStreamEncrypted::OutStreamEncrypted(int shift)
{
	this->shift = shift;
}

//Delete function for encrpyted stream out.
OutStreamEncrypted::~OutStreamEncrypted()
{
}

//Operator << for string (encrpyted stream out).
OutStreamEncrypted & OutStreamEncrypted::operator<<(const char * str)
{
	this->OutStream::operator<<(encrypt(str, this->shift));
	return *this;
}

//Operator << for number (encrpyted stream out).
OutStreamEncrypted & OutStreamEncrypted::operator<<(int num)
{
	char* str = new char;
	itoa(num, str, 10);
	this->OutStream::operator<<(encrypt(str, this->shift));
	return *this;
}

//Operator << for file (encrpyted stream out).
OutStreamEncrypted & OutStreamEncrypted::operator<<(void(*pf)(FILE *))
{
	this->OutStream::operator<<(*pf);
	return *this;
}

//Caeser cypher encrypt function by given shift.
char* OutStreamEncrypted::encrypt(const char str[], int shift) 
{
	char* temp = new char[sizeof(str) * 30];
	strncpy(temp, str, sizeof(str) * 30);

	for (int i = 0; temp[i] != 0; i++)
	{
		if (temp[i] + shift >= 32 && temp[i] + shift <= 126)
			temp[i] += (shift);
		else
			temp[i] += (shift - 25);
	}
	return temp;
}