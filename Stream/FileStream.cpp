#include "FileStream.h"
#include <stdlib.h>

//Initialazation function for file stream.
FileStream::FileStream(char* str)
{
	FILE * f = fopen(str, "w");
	if (!f)
	{
		printf("error can't open file.\n");
		system("pause");
		exit(0);
	}
	this->file = f;
}

//Delete function for file stream.
FileStream::~FileStream()
{
	fclose(this->file);
	this->file = nullptr;
}
